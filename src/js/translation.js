import "core-js/stable";
import "regenerator-runtime/runtime";

const SupportedExtensions = Object.freeze({
    "TXT": ".txt",
    "PPTX": ".pptx",
    "XLSX": ".xlsx",
    "DOCX": ".docx"
});

const MtJobStatus = {
    init: 'INIT',
    done: 'DONE',
    translating: 'TRANSLATING',
    failed: 'FAILED'
};

const MimeTypes = [{
    key: ".txt",
    value: "text/plain"
},
{
    key: ".docx",
    value: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
},
{
    key: ".xlsx",
    value: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
},
{
    key: ".pptx",
    value: "application/vnd.openxmlformats-officedocument.presentationml.presentation"
}];

const MtInputFormats = [{
    key: 'text/plain',
    value: 'PLAIN'
},
{
    key: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    value: 'DOCX'
},
{
    key: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    value: 'XLSX'
},
{
    key: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    value: 'PPTX'
}];

let selectedAttachment = null;
let selectedLanguagePair = null;

document.getElementById("translation-iframe").onload = async function () {
    let t = window.TrelloPowerUp.iframe();
    const tokenEntity = await t.get('member', 'private', 'tokenEntity');
    const accountId = await t.get('member', 'private', 'accountId');
    const languagePairEntity = await getAccountLanguagePairs(t, tokenEntity.mtToken.accessToken, accountId);

    const currentCard = await t.card('attachments', 'id');

    for (const key of currentCard.attachments) {

        let attachmentExtension = key.name.substr(key.name.lastIndexOf("."));

        if (attachmentExtension === SupportedExtensions.TXT ||
            attachmentExtension === SupportedExtensions.XLSX ||
            attachmentExtension === SupportedExtensions.DOCX ||
            attachmentExtension === SupportedExtensions.PPTX) {
            let listItem = document.createElement("li");

            let inputItem = document.createElement("input");
            inputItem.setAttribute("type", "checkbox");
            inputItem.addEventListener("change", onAttachmentSelect);

            listItem.appendChild(inputItem);
            listItem.appendChild(document.createTextNode(key.name));

            document.getElementById("attachments").appendChild(listItem);
        }
    }

    for (const key of languagePairEntity.languagePairs) {
        let listItem = document.createElement("li");

        let inputItem = document.createElement("input");
        inputItem.setAttribute("type", "checkbox");
        inputItem.addEventListener("change", onLanguagePairSelect);

        listItem.appendChild(inputItem);
        listItem.appendChild(document.createTextNode(key.sourceLanguageId + '-' + key.targetLanguageId + '-' + key.model));

        document.getElementById("languagePairs").appendChild(listItem);
    }

    document.getElementById("translateButton").onclick = async function () {
        await onTranslate(t, selectedAttachment, selectedLanguagePair, currentCard, tokenEntity);
    }
}

const onAttachmentSelect = (event) => {
    //TO-DO: Multiple checkings
    if (event.target.checked === true) {
        selectedAttachment = event.target.nextSibling.textContent;
        let attachments = document.getElementById("attachments").getElementsByTagName("li");
        for (const item of attachments) {
            if (item.lastChild.textContent != selectedAttachment) {
                item.firstChild.disabled = true;
            }
        }

        if (selectedLanguagePair) {
            document.getElementById("translateButton").disabled = false;
        }
    }

    if (event.target.checked === false) {
        selectedAttachment = null;
        let attachments = document.getElementById("attachments").getElementsByTagName("li");
        for (const item of attachments) {
            if (item.lastChild.textContent != selectedAttachment) {
                item.firstChild.disabled = false;
            }
        }
        document.getElementById("translateButton").disabled = true;
    }
}

const onLanguagePairSelect = (event) => {
    //TO-DO: Multiple checkings
    if (event.target.checked === true) {
        selectedLanguagePair = event.target.nextSibling.textContent;
        let languagePairs = document.getElementById("languagePairs").getElementsByTagName("li");
        for (const item of languagePairs) {
            if (item.lastChild.textContent != selectedLanguagePair) {
                item.firstChild.disabled = true;
            }
        }

        if (selectedAttachment) {
            document.getElementById("translateButton").disabled = false;
        }
    }

    if (event.target.checked === false) {
        selectedLanguagePair = null;
        let languagePairs = document.getElementById("languagePairs").getElementsByTagName("li");
        for (const item of languagePairs) {
            if (item.lastChild.textContent != selectedLanguagePair) {
                item.firstChild.disabled = false;
            }
        }
        document.getElementById("translateButton").disabled = true;
    }
}

const onTranslate = async (t, selectedAttachment, selectedLanguagePair, currentCard, tokenEntity) => {

    document.getElementById("translateButton").disabled = true;

    const currentAttachment = currentCard.attachments.filter(x => x.name == selectedAttachment)[0];
    const attachmentExtension = selectedAttachment.substr(selectedAttachment.lastIndexOf("."));
    let mimeType = null;
    for (const item of MimeTypes) {
        if (item.key == attachmentExtension) {
            mimeType = item.value;
            break;
        }
    }

    const languagePairModel = selectedLanguagePair.split('-');

    const attachmentContents = await getAttachmentContents(t, currentAttachment.url, mimeType);
    const translationJobResponse = await createTranslationJob(t, tokenEntity.mtToken.accessToken, languagePairModel[0], languagePairModel[1], mimeType, attachmentContents, selectedAttachment, languagePairModel[2]);

    let isJobFinished = false;

    while (!isJobFinished) {
        const jobStatusResponse = await retrieveTranslationJobStatus(t, tokenEntity.mtToken.accessToken, translationJobResponse.requestId);
        if (jobStatusResponse.translationStatus === MtJobStatus.failed) {
            return t.popup({
                title: 'Error',
                url: './error.html',
                args: {
                    message: 'SDL MT Cloud translation job failed! Please submit another job!'
                },
                height: 80
            });
        }

        if (jobStatusResponse.translationStatus === MtJobStatus.done) {
            isJobFinished = true;
        }
    }

    const translatedContents = await downloadTranslationJobContent(t, tokenEntity.mtToken.accessToken, translationJobResponse.requestId, mimeType);

    const newAttachmentName = currentAttachment.name.substring(0, currentAttachment.name.lastIndexOf(".")) +
        '_' + languagePairModel[1] + currentAttachment.name.substring(currentAttachment.name.lastIndexOf("."));

    const newAttachment = await createNewTrelloAttachment(t, tokenEntity.trelloApiKey, tokenEntity.trelloToken, currentCard.id, newAttachmentName, translatedContents, mimeType);

    if (newAttachment != undefined && newAttachment != null) {
        let listItem = document.createElement("li");

        let inputItem = document.createElement("input");
        inputItem.setAttribute("type", "checkbox");
        inputItem.disabled = true;
        inputItem.addEventListener("change", onAttachmentSelect);

        listItem.appendChild(inputItem);
        listItem.appendChild(document.createTextNode(newAttachment.name));

        document.getElementById("attachments").appendChild(listItem);
        document.getElementById("translateButton").disabled = false;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const getAttachmentContents = async (t, url, mimeType) => {
    let result;
    try {
        if (mimeType == 'text/plain') {
            result = await $.ajax({
                url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
                type: 'GET',
                dataType: 'text'
            });
            return result;
        }
        result = await $.ajax({
            url: 'https://cors-anywhere.herokuapp.com/' + url,
            type: 'GET',
            xhrFields: {
                responseType: 'blob'
            }
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'An error has occured while trying to retrieve the contents of the selected attachment!'
            },
            height: 80
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const createTranslationJob = async (t, accessToken, sourceLanguageId, targetLanguageId, mimeType, contents, attachmentName, model) => {

    const url = 'https://translate-api.sdlbeglobal.com/v4/mt/translations/async';

    const mtInputFormat = MtInputFormats.filter(x => x.key == mimeType)[0];

    const file = new File([contents], attachmentName, {
        type: mimeType,
    });

    const body = new FormData();
    body.append('sourceLanguageId', sourceLanguageId);
    body.append('targetLanguageId', targetLanguageId);
    body.append('model', model);
    body.append('inputFormat', mtInputFormat.value);
    body.append('input', file);

    let result;

    try {
        result = await $.ajax({
            url: 'https://cors-anywhere.herokuapp.com/' + url,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
            },
            contentType: false,
            processData: false,
            data: body,
            mimeType: 'multipart/form-data',
            dataType: "json"
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'An error has occured while trying to create the SDL MT Cloud project translation job!'
            },
            height: 80
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const retrieveTranslationJobStatus = async (t, accessToken, translationJobId) => {

    const url = `https://translate-api.sdlbeglobal.com/v4/mt/translations/async/${translationJobId}`;

    let result;

    try {
        result = await $.ajax({
            url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
            },
            dataType: "json"
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: `An error has occured while trying to retrieve the SDL MT Cloud project translation job status! Translation job id: ${translationJobId}.`
            },
            height: 80
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const downloadTranslationJobContent = async (t, accessToken, translationJobId, mimeType) => {

    const url = `https://translate-api.sdlbeglobal.com/v4/mt/translations/async/${translationJobId}/content`;

    let result;

    try {
        if (mimeType == 'text/plain') {
            result = await $.ajax({
                url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
                },
                dataType: 'text'
            });
            return result;
        }
        result = await $.ajax({
            url: 'https://cors-anywhere.herokuapp.com/' + url,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
            },
            xhrFields: {
                responseType: 'blob'
            }
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: `An error has occured while trying to retrieve the translated contents of the SDL MT Cloud translation job! Translation job id: ${translationJobId}.`
            },
            height: 120
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const createNewTrelloAttachment = async (t, trelloApiKey, trelloToken, cardId, attachmentName, contents, mimeType) => {

    const file = new File([contents], attachmentName, {
        type: mimeType,
    });

    const body = new FormData();
    body.append('file', file);

    const url = `https://api.trello.com/1/cards/${cardId}/attachments?key=${trelloApiKey}&token=${trelloToken}&name=${attachmentName}&mimeType=${mimeType}`;

    let result;

    try {
        result = await $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: body,
            mimeType: 'multipart/form-data',
            dataType: "json"
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: `An error has occured while trying to upload the new attachemnt to Trello. Attachment name: ${attachmentName}. MimeType: ${mimeType}.`
            },
            height: 120
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const getAccountLanguagePairs = async (t, accessToken, accountId) => {
    const url = `https://translate-api.sdlbeglobal.com/v4/accounts/${accountId}/subscriptions/language-pairs`;
    let result;

    try {
        result = await $.ajax({
            url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
            }
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'An error has occured while trying to retrieve the language pairs of your account! Please close the window and try again!'
            },
            height: 80
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

