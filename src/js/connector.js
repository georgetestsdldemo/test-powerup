import "core-js/stable";
import "regenerator-runtime/runtime";

const SDL_LOGO = 'https://upload.wikimedia.org/wikipedia/en/f/ff/SDL_logo.svg';

window.TrelloPowerUp.initialize({
    "card-buttons": function (t, options) {
        return {
            icon: SDL_LOGO,
            text: 'SDL MT Cloud',
            callback: onClickTrelloButton,
            condition: 'edit',
        }
    },
    "show-settings": function (t, options) {
        return t.popup({
            title: 'MT Cloud-Trello Configuration',
            url: "./configuration.html",
            height: 220,
        })
    }
});


const onClickTrelloButton = async (t) => {

    if (!t.memberCanWriteToModel('card')) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'You can\'t use this Power Up because you have no write permissions on the current card! Please contact the board\'s administrator!'
            },
            height: 100
        });
    }

    const tokenEntity = await t.get('member', 'private', 'tokenEntity');

    if (tokenEntity === undefined) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'You are not authenticated to SDL MT Cloud! Please visit the app settings and authenticate!'
            },
            height: 100
        });
    }

    if (Date.now() > tokenEntity.mtToken.expiresAt) {
        t.remove('member', 'private', ['tokenEntity', 'accountId']);
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'Your SDL MT Cloud token has expired. Please reauthenticate!'
            },
            height: 80
        });
    }

    const accessToken = tokenEntity.mtToken.accessToken;

    let accountId = await t.get('member', 'private', 'accountId');

    if (accountId === undefined) {
        accountId = await getAccountId(t, accessToken);
        if (accountId != undefined) {
            const languagePairs = await getAccountLanguagePairs(t, accessToken, accountId);
            if (languagePairs != undefined) {
                t.set('member', 'private', 'accountId', accountId);
                return t.popup({
                    title: 'SDL MT Cloud',
                    url: './translation.html',
                    height: 180
                });
            }
        }
    }
    else {
        return t.popup({
            title: 'SDL MT Cloud',
            url: './translation.html',
            height: 180
        });
    }
}

const getAccountId = async (t, accessToken) => {
    const url = 'https://translate-api.sdlbeglobal.com/v4/accounts/api-credentials/self';
    let result;

    try {
        result = await $.ajax({
            url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
            }
        });
        return result.accountId;
    }
    catch (error) {
        console.log(error);
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'An error has occured while trying to retrieve the accountId!'
            },
            height: 80
        });
    }
}

const getAccountLanguagePairs = async (t, accessToken, accountId) => {
    const url = `https://translate-api.sdlbeglobal.com/v4/accounts/${accountId}/subscriptions/language-pairs`;
    let result;

    try {
        result = await $.ajax({
            url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", `Bearer ${accessToken}`);
            }
        });
        return result;
    }
    catch (error) {
        return t.popup({
            title: 'Error',
            url: './error.html',
            args: {
                message: 'An error has occured while trying to retrieve the language pairs of your account!'
            },
            height: 80
        });
    }
}
