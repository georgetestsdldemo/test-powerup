document.getElementById('mt-cloud-configuration').onsubmit = (evt) => {
    let t = window.TrelloPowerUp.iframe();
    onInterceptMtCloudCredentials(evt, (mtToken, trelloToken, trelloApiKey) => {
        const tokenEntity = { mtToken: mtToken, trelloToken: trelloToken, trelloApiKey: trelloApiKey };
        t.set('member', 'private', 'tokenEntity', tokenEntity);
    });
}

const onInterceptMtCloudCredentials = (evt, storeMtCloudCredentialsCallback) => {
    evt.preventDefault();
    const formTextInputs = $('#' + evt.currentTarget.id + " :input:text");

    const clientId = formTextInputs.map(function () {
        if (this.name == 'clientId') {
            return this.value;
        }
    });

    const clientSecret = formTextInputs.map(function () {
        if (this.name == 'clientSecret') {
            return this.value;
        }
    });

    const trelloApiKey = formTextInputs.map(function () {
        if (this.name == 'trelloKey') {
            return this.value;
        }
    });
    const trelloDevToken = formTextInputs.map(function () {
        if (this.name == 'trelloToken') {
            return this.value;
        }
    });

    if (clientId[0] === '' || clientSecret[0] === '' || trelloDevToken[0] === '' || trelloApiKey[0] === '') {
        document.getElementById('confirmationMessage').setAttribute('style', 'display: block; margin-bottom: 5px; margin-left: 80px; color: red');
        document.getElementById('confirmationMessage').innerText = 'Missing input value!';
    }
    else {
        if (document.getElementById('confirmationMessage').innerText != '') {
            document.getElementById('confirmationMessage').innerText = '';
        }

        const url = 'https://translate-api.sdlbeglobal.com/v4/token';

        const credentials = {
            clientId: clientId[0],
            clientSecret: clientSecret[0]
        };

        $.ajax({
            url: 'https://sdl-cloud-proxy.herokuapp.com/' + url,
            type: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(credentials),
            success: function (token) {
                document.getElementById('confirmationMessage').setAttribute('style', 'display: block; margin-bottom: 5px; margin-left: 110px; color: green')
                document.getElementById('confirmationMessage').innerText = 'Success!';
                document.getElementById('authButton').className = '';
                document.getElementById('authButton').disabled = true;
                storeMtCloudCredentialsCallback(token, trelloDevToken[0], trelloApiKey[0]);
            },
            error: function (request, status) {
                document.getElementById('confirmationMessage').setAttribute('style', 'display: block; margin-bottom: 5px; margin-left: 75px; color: red');
                document.getElementById('confirmationMessage').innerText = request.responseText + '.' + status;
                console.log(request);
                console.log(status);
                console.log('salut!');
            }
        });
    }
}

const storeMtCloudCredentialsCallback = (token, trelloDevToken, trelloApiKey) => {
    return { token: token, trelloApiKey: trelloApiKey, trelloDevToken: trelloDevToken };
}